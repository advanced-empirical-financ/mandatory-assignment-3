# Load the required libraries
library(tidyverse)
#install.packages("alabama")
library(alabama) # Non-linear constrained minimizer  
library(RiskPortfolios) # used for LW shrinkage 
library(quadprog) # For no-short portfolio
library(kableExtra)
library("ggplot2")

# Exercise 1 -------------------------------------------------------------------

# First, load in the data sets
load("data_mandatory_assignment_3.rdata")

# Exteact the date column for later use
date <- returns %>%
  pull(date)

# Specify relevant parameters for evaluation
gamma <- 4 # risk aversion parameter
beta <- 99
N <- ncol(returns) -1 
T <- nrow(returns)


# We define the function "optimal_tc_weight" which calculates the optimal weights given the inputs, mu, Sigma, B, beta, gamma and  w_{t+}
optimal_tc_weight <- function(w_prev, 
                              mu, 
                              Sigma, 
                              B,
                              beta = 0, 
                              gamma = 4){
  # B is the diagonal matrix measuring the asset specific illiquidity
  
  N <- ncol(Sigma)
  iota <- rep(1, N)
  # Calculate the mu* (mu_proc) and Sigma* (Sigma_proc) which shrinks the estimation uncertainty
  Sigma_proc <- Sigma + beta / gamma * diag(B)
  mu_proc <- mu + beta *  diag(B) %*% w_prev # w_prev = w_{t+}
  
  # Invert Sigma*
  Sigma_inv <- solve(Sigma_proc)
  
  # Calculate the optimal portfolio weights that incorporates the transaction costs ex-ante (see equation (9) in Hautsch et al. p. 5)
  w_mvp <- Sigma_inv %*% iota
  w_mvp <- w_mvp / sum(w_mvp)
  w_opt <- w_mvp  + 1/gamma * (Sigma_inv - w_mvp %*% t(iota) %*% Sigma_inv) %*% mu_proc
  return(w_opt)
}

# Exercise 2 -------------------------------------------------------------------

# Compute the sample mu and the Ledoit Wolf sample Sigma
Sigma <- returns %>% 
  dplyr::select(-date) %>%
  as.matrix() %>%
  covEstimation(control = list(type = 'cor')) # Computes the Ledoit Wolf shrinkage covariance estimator

mu <- returns %>%
  dplyr::select(-date) %>%
  colMeans() 

# Extract the illiquidity of the individual assets as a matrix
B <- amihud_measures$illiquidity %>%
  as.vector()

# Cf. Proposition 4 in Hautsch et al (2019) the portfolio weight asympotically converges 
# towards a unique fix-point, which is calculated below
optimal_weight_theory <- function(mu, 
                                  Sigma, 
                                  beta = 0, 
                                  gamma = 4){
  # B is the diagonal matrix measuring the asset specific illiquidity
  
  N <- ncol(Sigma)
  iota <- rep(1, N)
  
  # Invert Sigma
  Sigma_inv <- solve(Sigma)
  
  # Calculate the optimal theoretical portfolio weights 
  w_mvp <- Sigma_inv %*% iota
  w_mvp <- w_mvp / sum(w_mvp)
  w_opt <- w_mvp  + 1/gamma * (Sigma_inv - w_mvp %*% t(iota) %*% Sigma_inv) %*% mu
  return(w_opt)
}

# Compute the theoretically optimal portfolio weights
w_theory <- optimal_weight_theory(mu, Sigma, beta = beta, gamma = gamma) 

w_theory <- w_theory %>% as.data.frame()

# Make the optimal portfolio weights ready for plotting
w_theory <- w_theory %>% 
  mutate(date = last(returns$date)) %>%
  rename(w_theory = "V1") %>%
  pivot_longer(-date, names_to = "ticker", values_to = "weight")


# Create an empty tibble to store the converging portfolio weights in
periods <- nrow(returns)
w_portf_conv <- matrix(NA, 
                       nrow = nrow(returns),
                       ncol = ncol(returns)) # A matrix to collect all returns
colnames(w_portf_conv) <- colnames(returns)
w_portf_conv <- w_portf_conv[,-1] # drop the empty date column 

# Assume that the initial portfolio weights (w_0) are given by the naïve portfolio
N <- ncol(Sigma)
weight <- rep(1/N ,N)

for(i in 1:periods){
  # Compute and store the portfolio weights for period i
  weight <- optimal_tc_weight(w_prev = weight, mu = mu, Sigma = Sigma, B = B, beta = beta, gamma = gamma)
  w_portf_conv[i,] <- weight
}

# Merge the converging portfolio weights with the date column and 
w_portf <- cbind.data.frame(returns$date, w_portf_conv) %>%
  rename(date = "returns$date") %>%
  pivot_longer(-date, names_to = "ticker", values_to = "weight") 

# Plot the portfolio weight convergence
portf_w_convergence <- w_portf %>%
  ggplot(aes(x = date, y = weight, group = ticker, color = ticker)) +
  geom_line() +
  # geom_text(data = w_portf  %>% filter(date == last(date) & ticker == c('CZNC', 'AEL')), 
  #           aes(label = ticker, color = ticker, x = last(date), y = weight), vjust = -.2) +
  geom_point(data = w_theory, aes(x = date, y = weight), color = "black") + # the black line indicates the theoretically optimal weight +
  labs(x = "", y = "Portfolio weight") +
  theme_minimal() 

portf_w_convergence

# Save the figure
#ggsave(file="./portf_w_convergence.png", portf_w_convergence)

# Exercise 3 -------------------------------------------------------------------

# Illustrate effect of Turnover penalization
returns_turnover <- returns %>% select(-date)

Sigma_turnover <-  returns_turnover %>% as.matrix() %>%
  covEstimation(control = list(type = 'cor')) # Computes the Ledoit Wolf shrinkage covariance estimator

N <- ncol(Sigma_turnover)

mu_turnover <-  0 * colMeans(returns_turnover)

# LW portfolio
beta_concentration_LW <- function(beta){
  return(sum((optimal_tc_weight(rep(1/N, N), mu_turnover, Sigma_turnover, B=B, beta = beta, gamma = 4) - rep(1/N, N))^2))
}

beta_effect_LW <- tibble(beta = 10000 * qexp((1:99)/100)) %>% 
  mutate(concentration = map_dbl(beta, beta_concentration_LW))


# Naive portfolio
#beta_concentration_naive <- function(beta){
#return(rep(1/N, N))
#}

#beta_effect_naive <- tibble(beta = 10000 * qexp((1:99)/100)) %>% 
#mutate(concentration = map_dbl(beta, beta_concentration_naive))

# Buy-and-hold portfolio
beta_concentration_BH <- function(beta){
  return(sum((rep(1/N, N)) - rep(1/N, N))^2)
}

beta_effect_BH <- tibble(beta = 10000 * qexp((1:99)/100)) %>% 
  mutate(concentration = map_dbl(beta, beta_concentration_BH))


# Plot 

beta_effect_LW %>% 
  ggplot(aes(x = beta, y = concentration)) + 
  geom_line()
labs(x = "Transaction cost parameter", 
     y = "Rebalancing")

beta_effect_BH %>% 
  ggplot(aes(x = beta, y = concentration)) + 
  geom_line()
labs(x = "Transaction cost parameter", 
     y = "Rebalancing")

# Now, the Sharpe Ratios are calculated

# Select dates
returns <- returns %>% select(-date) 
returns <- returns/100

# OOS experiment

window_length <- 250

# Define number of out-of-sample periods
periods <- nrow(returns) - window_length 

# Define matrix to collect all returns 
oos_values <- matrix(NA, 
                     nrow = periods, 
                     ncol = 3) 

colnames(oos_values) <- c("raw_return", "turnover", "net_return") 

all_values <- list(oos_values)


# Define initial allocation
w_prev_Sharpe <- rep(1/N, N)

# Estimate rolling window

Sharpe_rolling <- function(beta){
  
  for(i in 1:periods) {
    
    # Extract information: The last x returns available up to date t
    return_window <- returns[i : (i + window_length - 1), ]
    
    # Estimate sample moments 
    sigma <- return_window %>% as.matrix() %>%
      covEstimation(control = list(type = 'cor')) # Computes the Ledoit Wolf shrinkage covariance estimator 
    mu <- colMeans(return_window)
    
    ### Construct optimal TC robust portfolio ###
    w_1 <- optimal_tc_weight(w_prev = w_prev_Sharpe, mu = mu, Sigma = Sigma, B=B, beta = beta, gamma = gamma)
    
    # Evaluation 
    raw_return <- returns[i + window_length, ] %>% as.matrix() %*% w_1
    turnover <- sum((as.vector(w_1)-as.vector(w_prev_Sharpe))^2)
    
    #Store values 
    net_return <- raw_return - beta * turnover
    all_values[[1]][i, ] <- c(raw_return, turnover, net_return)
    
  }
  
  Mean <- all_values[[1]] %>% mean(net_return)
  SD <- all_values[[1]] %>% sd(net_return)
  Sharpe <- Mean/SD
  
  
  return(Sharpe)
}



# Define matrix to collect all Sharpe Ratios

Sharpe_values <- matrix(NA, 
                        nrow = 100, 
                        ncol = 1) 

colnames(Sharpe_values) <- c("Sharpe") 

Sharpe_range <- list(Sharpe_values)

# Loop over range of betas

for(i in 0:100) {
  Sharpe_range[[1]][i, ] <- Sharpe_rolling(i)
}


#Plot Sharpe ratios

sharpe_ranges <- Sharpe_range[[1]] %>% as.tibble()

sharpe_ranges %>%
  ggplot(aes(x = 1:100 , y = Sharpe)) + 
  geom_line() + 
labs(x = "beta", 
     y = "Sharpe ratio") +
  theme_minimal()



# Exercise 4 -------------------------------------------------------------------
# This section computes implements a full-fledged portfolio backtesting strategy 
# and compares it to the naive (adaptively updating)
# portfolio and the no-shortselling portfolio. 


returns <- cbind(date, returns)

# Set window length 
window_length <- 250
periods <- nrow(returns) - window_length # total number of out-of-sample periods

beta <- 50/10000 # Transaction costs, 50bp
oos_values <- matrix(NA, 
                     nrow = periods, 
                     ncol = 3) # A matrix to collect all returns
colnames(oos_values) <- c("raw_return", "turnover", "net_return") 

all_values <- list(oos_values, 
                   oos_values,
                   oos_values)

w_prev_1 <- w_prev_2 <- w_prev_3 <- rep(1/N ,N)  # Set stating values to be naive portfolio


# Loop over periods and iteratively compute portfolio weights 

invisible(capture.output(for(i in 1:periods){ # Rolling window invisible(capture.output())
  #browser()
  return_window <- returns[i : (i + window_length - 1),] %>% dplyr::select(-date) # the last X returns available up to date t
  
  # LW shrinkage estiamtor 
  Sigma <- covEstimation(return_window %>% as.matrix(), control = list(type = 'cor'))
  mu <- colMeans(return_window) %>% as.matrix()
  
  
  ### Under L1 transaction costs
  # Compute portfolio weights using alabama::constrOptim.nl
  # Does not appear to be correct
  fn <- function(w){ # objective function 
    omega_star <- -1*( t(w)%*%mu - beta* sum(abs(w - w_prev_1)) -
                         (gamma/2) * (t(w) %*% Sigma %*% w)) #constrOptim.nl  minimizes, therefore -1
    return(omega_star)}
  
  # Equality contraint, w'iota=1
  heq <- function(w){
    h <- rep(NA,1)
    h[1] <- t(w) %*% rep(1,N) - 1 # Defined to equal 0 in equality 
    return(h)
  }
  
  w_1 <- alabama::constrOptim.nl(par = w_prev_1, 
                                 fn = fn,
                                 heq = heq)
  w_1 <- w_1$par
  # Evaluation
  raw_return <- returns[i + window_length, ] %>% dplyr::select(-date) %>% as.matrix() %*% w_1
  
  turnover <- sum(abs(as.vector(w_1)-as.vector(w_prev_1))) # as in eq. (46) - the L1 norm (Taxicab norm, assumed)
  # Store realized returns
  net_return <- raw_return - beta * turnover
  
  all_values[[1]][i,] <- c(raw_return, turnover, net_return) # store values 
  
  #Computes adjusted weights based on the weights and next period returns
  w_prev_1 <- w_1 * as.vector(1 + returns[i + window_length, ] %>% select(-date) / 100)
  w_prev_1 <- w_prev_1 / sum(as.vector(w_prev_1))
  w_prev_1 <- t(w_prev_1)
  
  ### Naive Portfolio 
  w_2 <- rep(1/N, N)
  
  # Evaluation
  raw_return <- returns[i + window_length, ] %>% dplyr::select(-date) %>% as.matrix() %*% w_2
  
  turnover <- sum(abs(as.vector(w_2)-as.vector(w_prev_2))) # as in eq. (46)
  # Store realized returns
  net_return <- raw_return - beta * turnover
  
  
  all_values[[2]][i,] <- c(raw_return, turnover, net_return)
  #Computes adjusted weights based on the weights and next period returns
  w_prev_2 <- w_2 * as.vector(1 + returns[i + window_length, ] %>% select(-date) / 100)
  w_prev_2 <- w_prev_2 / sum(as.vector(w_prev_2))
  
  
  ### No-short portfolio
  
  # Compute portfolio weights
  A_ns <- cbind(1,diag(N)) 
  b_ns <- c(1,rep(0,N))
  
  # Calculate the efficient portfolio weights which insure the desired return mu_bar
  # and impose the constraints of no short-selling
  
  # Need a predetermined mu_bar -> set equal to zero 
  mu_bar <- 0
  w <- solve.QP(Dmat = Sigma,
                dvec = rep(0,N),
                Amat = cbind(A_ns,mu), # add condition for mu 
                bvec = c(b_ns,mu_bar), # ensures desired return, no short, limited exposure
                meq  = 1) #only equality constraint is sum of weights is 1  
  
  # Extract the efficient portfolio weight under the imposed portfolio constraints 
  w_3 <- w$solution
  
  # Evaluation
  raw_return <- returns[i + window_length, ] %>% dplyr::select(-date) %>% as.matrix() %*% w_3
  
  turnover <- sum(abs(as.vector(w_3)-as.vector(w_prev_3))) # as in eq. (46)
  # Store realized returns
  net_return <- raw_return - beta * turnover
  
  all_values[[3]][i,] <- c(raw_return, turnover, net_return) # store values 
  
  #Computes adjusted weights based on the weights and next period returns
  w_prev_3 <- w_3 * as.vector(1 + returns[i + window_length, ] %>% select(-date) / 100)
  w_prev_3 <- w_prev_3 / sum(as.vector(w_prev_3))
  
}))  # might want to wrap in invisible() (or similar to supress output)


# Next compute OOS Sharpe ratio net of transaction costs and average turnover 
all_values <- lapply(all_values, as_tibble) %>% bind_rows(.id = "strategy")
options(knitr.table.format = "latex")
all_values %>%
  group_by(strategy) %>%
  summarise(Mean =250* mean(net_return), 
            SD = sqrt(250)*sd(net_return),
            Sharpe = Mean/SD,
            Turnover = 100* mean(turnover)) %>%
  mutate(strategy = case_when(strategy == 1 ~ "MV (TC)",
                              strategy == 2 ~ "Naive", 
                              strategy == 3 ~ "MV no-short")) %>% 
  knitr::kable(digits = 4)

