#######################################################################
#                                                                     #
#                     Advanced Empirical Finance:                     #
#                     Mandatory Assignment #3                         #
#                                                                     #
#######################################################################
beta_values <- 21.5 * qexp((1:99)/100) # Transaction costs

################# 0. Initial settings for the script ##################
rm(list = ls()) # Clearing the workspace

beta_values <- 21.5 * qexp((1:99)/100) # Transaction costs


library(tidyverse) # Loading in main package for data manipulations
library(lubridate) # Loading in package to transform dates
library(MASS) # Loading in package required to use mvrnorm
library(kableExtra) # Loading package to create tables
library(quadprog) # Loading package to use solve.QP


############################ Exercise 1 ##############################
#Read in the data using the load function
load("data_mandatory_assignment_3.rdata")

returns <- returns %>%  dplyr::select(-date)
returns <- returns/100


mu <- returns %>% #Sample estimate of mu
  colMeans() %>% 
  as.matrix() #Converting to a matrix

sigma <- returns %>% #Sample estimate of variance-covariance matrix
  cov(use = "pairwise.complete.obs") #Variance co-variance matrix

compute_ledoit_wolf <- function(x) {
  # Computes Ledoit-Wolf shrinkage covariance estimator
  # This function generates the Ledoit-Wolf covariance estimator as proposed in Ledoit, Wolf 2004 (Honey, I shrunk the sample covariance matrix.)
  # X is a (t x n) matrix of returns
  t <- nrow(x)
  n <- ncol(x)
  x <- apply(x, 2, function(x) if (is.numeric(x)) # demean x
    x - mean(x) else x)
  sample <- (1/t) * (t(x) %*% x)
  var <- diag(sample)
  sqrtvar <- sqrt(var)
  rBar <- (sum(sum(sample/(sqrtvar %*% t(sqrtvar)))) - n)/(n * (n - 1))
  prior <- rBar * sqrtvar %*% t(sqrtvar)
  diag(prior) <- var
  y <- x^2
  phiMat <- t(y) %*% y/t - 2 * (t(x) %*% x) * sample/t + sample^2
  phi <- sum(phiMat)
  
  repmat = function(X, m, n) {
    X <- as.matrix(X)
    mx = dim(X)[1]
    nx = dim(X)[2]
    matrix(t(matrix(X, mx, nx * n)), mx * m, nx * n, byrow = T)
  }
  
  term1 <- (t(x^3) %*% x)/t
  help <- t(x) %*% x/t
  helpDiag <- diag(help)
  term2 <- repmat(helpDiag, 1, n) * sample
  term3 <- help * repmat(var, 1, n)
  term4 <- repmat(var, 1, n) * sample
  thetaMat <- term1 - term2 - term3 + term4
  diag(thetaMat) <- 0
  rho <- sum(diag(phiMat)) + rBar * sum(sum(((1/sqrtvar) %*% t(sqrtvar)) * thetaMat))
  
  gamma <- sum(diag(t(sample - prior) %*% (sample - prior)))
  kappa <- (phi - rho)/gamma
  shrinkage <- max(0, min(1, kappa/t))
  if (is.nan(shrinkage))
    shrinkage <- 1
  sigma <- shrinkage * prior + (1 - shrinkage) * sample
  return(sigma)
}
sigma_LW <- compute_ledoit_wolf(returns)


#Fix the relevant parameters
gamma <- 4 #The risk aversion parameter is assumed to be four throughout the entire assignment
ticker <- colnames(returns) #Helping step to find the number of assets
N <- length(ticker) #Finding the number of assets
B <- diag(as_vector((amihud_measures[,2]))) #Transform illiquidity to a diagonal matrix 

#Function for the optimal weights:
optimal_tc_weight <- function(mu, 
                              Sigma,
                              gamma=4,
                              beta, 
                              B,
                              w_curr
){ #Specification of inputs
  N <- ncol(Sigma) #Number of assets is equal to the number of columns in Sigma
  iota <- rep(1, N)  #A vector of ones with N elements 
  Sigma_proc <- Sigma + ((beta / gamma) * B) #Specification of the Sigma-parameter used for optimization
  mu_proc <- mu + beta * B %*% w_curr #Specification of the mu-parameter used for optimization
  
  Sigma_inv <- solve(Sigma_proc) #Inverting Sigma_proc for below calculation
  
  w_mvp <- Sigma_inv %*% iota #Find minmium variance portfolio weights
  w_mvp <- w_mvp / sum(w_mvp) #Normalization of minimum variance weights
  w_opt <- w_mvp  + 1/gamma * (Sigma_inv - w_mvp %*% t(iota) %*% Sigma_inv) %*% mu_proc #Find optimal portfolio weights
  return(w_opt) #Return the optimal portfolio weights
}

############################ Exercise 2 ##############################

##### For the actual illiquidity matrix B
iterations = 100000 # Number of reallocations
beta = 100 # Transaction costs


# Initialization
weights <- matrix(NA,nrow=iterations,ncol=41) # Matrix to store weights for each reallocation
portfolios <- matrix(NA, nrow = iterations,ncol=2) # Matrix to store return and volatility of each reallocation

colnames(weights) <- c(ticker, "time")
colnames(portfolios) <- c("mu","sd") # Specify column names

# First iteration is set to the naive portfolio
w_naive <- rep(1/N,N) %>% as.matrix() # Calculate the naive portfolio
weights[1,1:40] <- t(w_naive) # Add the naive portfolio as the first set of weights

#loop over weight to compute convergence of portfolio weights
for(i in 1:(iterations-1)){
  row = i + 1
  w_before <- i
  weights[row,1:40] <- t(optimal_tc_weight(mu, sigma, gamma=4, beta = beta, B, weights[w_before,1:40]))
  }


# loops over weights to compute portfolio returns and std. dev.
for(i in 1:iterations){
  portfolios[i,1] = sum(mu * weights[i,1:40])
  portfolios[i,2] = sqrt(t(weights[i,1:40]) %*% sigma %*% weights[i,1:40]) 
}

# Add column with time for reallocation
weights[1:iterations,41] <- seq(1,iterations, by=1)
weights <- as_tibble(weights) # Convert to tibble

# The quartiles of Amihud measure
amihud_measures <- amihud_measures %>% mutate(quartile = ntile(illiquidity, 4))
quartiles <- amihud_measures[1:40,3]

# replication of quartiles
rep_quartiles <- do.call("rbind", replicate(iterations, quartiles, simplify = FALSE))

# Pivot of data in order to make plot
weights_longer <- pivot_longer(
  weights,
  all_of(ticker),
  names_to = "ticker") 

# add column with quartiles of amihud measure to tibble of iterated weights
Weight_Dynamics <- cbind(weights_longer,rep_quartiles)


portfolios <- portfolios %>% as_tibble()


compute_efficient_frontier <- function(mu, 
                                       sigma,
                                       gamma = 4
) {
  
  
  N <- length(mu) #Arbitrary number of portfolios
  iota <- rep(1, N) #Defining a vector of ones
  
  sigma_inv <- solve(sigma)
  
  w_mvp <- sigma_inv %*% iota #Solving for minimum variance weights using that %*% is matrix multiplicatin in R
  w_mvp <- w_mvp / sum(w_mvp) #Normalizing to find minimum variance portfolio weight
  w_opt <- w_mvp  + 1/gamma * (sigma_inv - w_mvp %*% t(iota) %*% sigma_inv) %*% mu
  
  c <- seq(from = -0.4, to = 1.5, by = 0.01)
  res <- tibble(c = c, 
                mu = NA,
                sd = NA)
  for(i in seq_along(c)){ # For loop
    w <- (1-c[i])*w_mvp + (c[i])*w_opt # Using two mutual fund theorem to characterize the efficient frontier
    res$mu[i] <- t(w) %*% mu # Portfolio expected return using true value of mu
    res$sd[i] <- sqrt(t(w) %*% sigma %*% w) # Portfolio volatility using true value of sigma
  }
  return(res) #Returning a tibble with c, mu and sd
}

# Visualize efficient frontier
Efficient_Frontier <- ggplot(compute_efficient_frontier(mu, sigma), aes(x = sd, y = mu)) + 
  geom_point(size=0.1) + # Plot all sd/mu portfolio combinations from the function above
  geom_point(data = compute_efficient_frontier(mu, sigma) %>% filter(c %in% c(1)), 
             color = "red",
             size = 2) + # locate efficient portfolio
  geom_point(data = portfolios, 
             aes(y = mu, x  = sd), color = "blue", size = 0.5) +
  labs(x = "Volatility", y = "Expected Return") + #Renaming x-axis and y-axis to more meaningful names
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  ) +
  scale_x_continuous(breaks = scales::pretty_breaks(n = 5), labels = function(x) paste0(x)) +
  scale_y_continuous(breaks = scales::pretty_breaks(n = 8), labels = function(x) paste0(x)) +
  expand_limits(x = c(0,0.1), y = c(0,0.02))

##### For the identity matrix I
iterations = 100000 # Number of reallocations
beta = 100 # Transaction costs


# Initialization
weights_id <- matrix(NA,nrow=iterations,ncol=41) # Matrix to store weights for each reallocation
portfolios_id <- matrix(NA, nrow = iterations,ncol=2) # Matrix to store return and volatility of each reallocation

colnames(weights_id) <- c(ticker, "time")
colnames(portfolios_id) <- c("mu","sd") # Specify column names

# First iteration is set to the naive portfolio
w_naive <- rep(1/N,N) %>% as.matrix() # Calculate the naive portfolio
weights_id[1,1:40] <- t(w_naive) # Add the naive portfolio as the first set of weights

#loop over weight to compute convergence of portfolio weights
for(i in 1:(iterations-1)){
  row = i + 1
  w_before <- i
  weights_id[row,1:40] <- t(optimal_tc_weight(mu, sigma, gamma=4, beta = beta, diag(N), weights_id[w_before,1:40]))
}


# loops over weights to compute portfolio returns and std. dev.
for(i in 1:iterations){
  portfolios_id[i,1] = sum(mu * weights_id[i,1:40])
  portfolios_id[i,2] = sqrt(t(weights_id[i,1:40]) %*% sigma %*% weights_id[i,1:40]) 
}

# Add column with time for reallocation
weights_id[1:iterations,41] <- seq(1,iterations, by=1)
weights_id <- as_tibble(weights_id) # Convert to tibble

portfolios_id <- portfolios_id %>% as_tibble()

# Visualize efficient frontier for the case with the identity matrix
Efficient_Frontier_Id <- ggplot(compute_efficient_frontier(mu, sigma), aes(x = sd, y = mu)) + 
  geom_point(size=0.1) + # Plot all sd/mu portfolio combinations from the function above
  geom_point(data = compute_efficient_frontier(mu, sigma) %>% filter(c %in% c(1)), 
             color = "red",
             size = 2) + # locate efficient portfolio
  geom_point(data = portfolios_id, 
             aes(y = mu, x  = sd), color = "blue", size = 0.5) +
  labs(x = "Volatility", y = "Expected Return") + #Renaming x-axis and y-axis to more meaningful names
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  ) +
  scale_x_continuous(breaks = scales::pretty_breaks(n = 5), labels = function(x) paste0(x, "%")) +
  scale_y_continuous(breaks = scales::pretty_breaks(n = 8), labels = function(x) paste0(x, "%")) +
  expand_limits(x = c(0,0.1), y = c(0,0.02))




### Split the data in quartiles with respect to the Amihud Measure
Q1_Figure <- Weight_Dynamics %>% filter(quartile==1)
Q2_Figure <- Weight_Dynamics %>% filter(quartile==2)
Q3_Figure <- Weight_Dynamics %>% filter(quartile==3)
Q4_Figure <- Weight_Dynamics %>% filter(quartile==4)


# Plot for Q1
Q1_Figure <- ggplot(Q1_Figure, aes(x = time, y = value, colour=ticker, )) + 
  geom_line(show.legend = FALSE) +
  labs(x = "Number of iterations", 
       y = "Q1") + 
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  )

# Plot for Q2
Q2_Figure <- ggplot(Q2_Figure, aes(x = time, y = value, colour=ticker, )) + 
  geom_line(show.legend = FALSE) +
  labs(x = "Number of iterations", 
       y = "Q2") + 
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  )

# Plot for Q3
Q3_Figure <- ggplot(Q3_Figure, aes(x = time, y = value, colour=ticker, )) + 
  geom_line(show.legend = FALSE) +
  labs(x = "Number of iterations", 
       y = "Q3") + 
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  )

# Plot for Q4
Q4_Figure <- ggplot(Q4_Figure, aes(x = time, y = value, colour=ticker, )) + 
  geom_line(show.legend = FALSE) +
  labs(x = "Number of iterations", 
       y = "Q4") + 
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  )

# Plot of the convergence for each group
#Convergence_Quartiles_Plot <- grid.arrange(Q1_Figure, Q2_Figure, Q3_Figure, Q4_Figure, ncol=2)


############################ Exercise 3 ##############################

#Compute out of sample returns for each values of beta using a rolling window and substract the corresponding transaction costs.
window_length <- 250 #Set the window length to 250
periods <- nrow(returns) - window_length # total number of out-of-sample periods

# Used to store results
oos_values <- matrix(NA, 
                     nrow = periods, 
                     ncol = 3) # A matrix to collect all returns
colnames(oos_values) <- c("raw_return", "turnover", "net_return") # we implement 3 strategies

# List of tibbles for oos values
all_values <-list(oos_values, oos_values)




w_prev_1 <- w_prev_2 <- rep(1/N ,N) #Initial weights is the naive portfolio.

# Store betas and sharpe ratios
result_ex3 <- tibble(beta = beta_values,
                Sharpe_lw = NA,
                Sharpe = NA)


# Computation of out-of-sample returns series for a particular value of beta
Calculate_oos_sharpe <- function(beta_values){
  
  for(j in seq_along(beta_values)){
    
    # rolling window
    for(i in 1:periods){
      
      return_window <- returns[i : (i + window_length - 1),]
      
      
      
      # Parameters 
      Sigma <- cov(return_window) 
      Sigma_lw <- compute_ledoit_wolf(return_window)
      mu <- 0*colMeans(return_window) 

      # Optimal TC robust portfolio
      w_1 <- optimal_tc_weight(mu = mu, Sigma = Sigma_lw, gamma=gamma, beta=beta_values[j], B=B, w_curr=w_prev_1)
     
      
      # Evaluation
      raw_return <- as.matrix(returns[i + window_length, ]) %*% as.matrix(w_1)
      turnover <- t(as.vector(w_1)-as.vector(w_prev_1)) %*% B %*%   (as.vector(w_1)-as.vector(w_prev_1))
      
      
      # store realized returns
      net_return <- raw_return - beta_values[j] * turnover
      
      # store out-of-sample results in tibble
      all_values[[1]][i,] <- c(raw_return, turnover, net_return)
      
      #Computes adjusted weights based on the weights and next period returns
      w_prev_1 <- w_1 * as.vector(1 + (returns[i + window_length, ]))
      w_prev_1 <- as.numeric(w_prev_1 / sum(as.vector(w_prev_1)))
      
      
      #### Same calculations but using the sample variance-covariance matrix
      
      # optimal transaction cost robust portfolio
      w_2 <- optimal_tc_weight(mu = mu, Sigma = Sigma, gamma=gamma, beta=beta_values[j], B=B, w_curr=w_prev_1)
      
      # Evaluation
      raw_return <- as.matrix(returns[i + window_length, ]) %*% as.matrix(w_2)
      turnover <- t(as.vector(w_2)-as.vector(w_prev_2)) %*% B %*% (as.vector(w_2)-as.vector(w_prev_2))
      
      # store realized returns
      net_return <- raw_return - beta_values[j] * turnover
      
      # store out-of-sample results in tibble
      all_values[[2]][i,] <- c(raw_return, turnover, net_return)
      
      #Computes adjusted weights based on the weights and next period returns
      w_prev_2 <- w_2 * as.vector(1 + (returns[i + window_length, ]))
      w_prev_2 <- as.numeric(w_prev_2 / sum(as.vector(w_prev_2)))
    }
    # Store calculations
    all_values_new <- lapply(all_values, as_tibble) %>% bind_rows(.id = "strategy")
    
    # Calculate Mean, Standard Deviation, Sharpe Ratio and Turnover
    all_values_new <- all_values_new %>%
      group_by(strategy) %>%
      summarise(Mean = 250*mean(net_return),
                SD = sqrt(250)*sd(net_return),
                Sharpe = Mean/SD,
                Turnover = 100 * mean(turnover))
    
    # Store results in a tibble
    result_ex3$Sharpe_lw[j] <- all_values_new$Sharpe[1]
    result_ex3$Sharpe[j] <- all_values_new$Sharpe[2]
  
  }
  return(result_ex3)
}

#Use function above in a for loop over different values of beta
beta_values <- 21.5 * qexp((1:99)/100) # Transaction costs

Sharpe_Ratios <-  Calculate_oos_sharpe(beta_values)

Sharpe_Plot_ex3 <- ggplot() + 
  geom_line(data = Sharpe_Ratios, 
             aes(y = Sharpe_lw, x  = beta, color = "blue"), size = 1) +
  labs(x = "Beta", y = "Sharpe Ratio") + #Renaming x-axis and y-axis to more meaningful names
  geom_line(data = Sharpe_Ratios, aes(y = Sharpe, x= beta, color="black"),size=1) +
  scale_color_discrete(name = "Estimator", labels = c("Ledoit Wolf", "Sample")) +
  labs(x = "beta", y = "Annualized Sharpe Ratio") +
  theme_bw() + #Making the plot prettier
  theme(axis.text.y   = element_text(size=10),
        axis.text.x   = element_text(size=10),
        axis.title.y  = element_text(size=10),
        axis.title.x  = element_text(size=10),
        panel.background = element_blank(),
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
  )

############################ Exercise 4 ##############################