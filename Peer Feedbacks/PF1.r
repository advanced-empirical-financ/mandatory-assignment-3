library(alabama)
library(quadprog)
library(knitr) # knitr and dplyr are only used for the Latex table at the bottom
library(dplyr) 
rm(list=ls())
load("data_mandatory_assignment_3.RData")

# More comments will be provided for the exam :-)

logReturns <- data.matrix(returns[-1]/100)
capT <- nrow(logReturns)
N <- ncol(logReturns)
dates <- returns[1]

# 1. Optimal weights function
weights <- function(t,w,mu,Sigma,gamma,beta,B,iota,returns=T) {
  
  if (returns) {
    tmp <- w*(1+logReturns[t,])
    wplus <- w*tmp/(t(iota) %*% tmp)[1]
  } else wplus <- w 
  
  muB <- mu + beta*B %*% wplus
  SigmaB <- beta/gamma*B + Sigma
  
  SigmaBinv <- solve(SigmaB)
  tmp2 <- t(iota) %*% SigmaBinv
  nom <- 1-(tmp2 %*% muB)[1]/gamma
  denom <- (tmp2 %*% iota)[1]/gamma
  
  SigmaBinv %*% (muB + nom/denom*iota)/gamma
}

# 2. Convergence
gamma <- 4
mu <- matrix(rep(0,N),nrow=N)
Sigma <- cov(logReturns)
iota <- matrix(1,nrow=length(mu))
B <- diag(N)
diag(B) <- as.matrix(amihud_measures[-1])

convergence <- function(beta) {
  wt <- matrix(rep(rep(0,N),capT+1),nrow=capT+1,ncol=N)
  absDiff <-rep(0,capT)
  wt[1,] <- 1/N*matrix(1,nrow=N)
  for (t in 1:capT) {
    wt[t+1,] <- weights(t,wt[t,],mu,Sigma,gamma,beta,B,iota,F)
    absDiff[t] <- sum(abs(wt[t+1,]-wt[t,]))
  }
  absDiff
}

betas <- c(10,20,40,80)
nb <- length(betas)
absDiffs <- matrix(rep(rep(0,capT),nb),ncol=nb)
for (i in 1:nb) {
  absDiffs[,i] <- convergence(betas[i])
}
colors <- c("blue","red","darkgoldenrod1","forestgreen")
interval <- 1:100
plot(interval,absDiffs[interval,1],
     xlab='Number of iterations',ylab='Sum of absolute changes in w',col=colors[1],
     ylim=c(min(absDiffs),max(absDiffs)))
for (i in 2:4) points(interval,absDiffs[interval,i],col=colors[i])
legend("topright", 
      legend = c("Beta = 10","Beta = 20","Beta = 40","Beta = 80"), 
      col = c("blue","red","darkgoldenrod1","forestgreen"), 
      pch = c(19,19,19,19), 
      bty = "n", 
      pt.cex = 1, 
      cex = 1.0, 
      text.col = "black", 
      horiz = F , 
      inset = c(0.1,0.1,0.1,0,1))

illuidityFocused <- function(beta) {
  
  ordering <- order(diag(B))
  diag(B) <- diag(B)[ordering]
  
  # Note: Returns do not enter in this part of the assignment. 
  #Hence, we only adjust the ordering of Sigma
  Sigma <- cov(logReturns[,ordering])
  
  wt <- matrix(rep(rep(0,N),capT+1),nrow=capT+1,ncol=N)
  liquid <- illiquid <-rep(0,capT)
  wt[1,] <- 1/N*matrix(1,nrow=N)
  for (t in 1:capT) {
    wt[t+1,] <- weights(t,wt[t,],mu,Sigma,gamma,beta,B,iota,F)
    liquid[t] <- sum(abs(wt[t+1,1:5]-wt[t,1:5]))
    illiquid[t] <- sum(abs(wt[t+1,35:40]-wt[t,35:40]))
  }
  list(liquid,illiquid)
}

betas <- c(10,80)
nb <- length(betas)
liquid <- illiquid <- matrix(rep(rep(0,capT),nb),ncol=nb)
for (i in 1:nb) {
  L <- illuidityFocused(betas[i])
  liquid[,i] <- L[[1]]
  illiquid[,i] <- L[[2]]
}
colors <- c("red","darkred","royalblue","blue")
interval <- 1:500
axisill <- c(min(illiquid),max(illiquid))
axisl <- c(min(liquid),max(liquid))
plot(interval,illiquid[interval,1],
     xlab='Number of iterations',ylab='Sum of absolute changes in w',col=colors[1],ylim=axisill)
points(interval,illiquid[interval,2],col=colors[2])
text(250,0.00014,"Left axis: Top 5 most illiuqid (red-ish)\n Right axis: Top 5 most liquid (blue-ish)")
text(250,0.0001,"Dark colors: Beta = 80 \n Light colors: Beta = 10")

par(new=TRUE)
plot(interval,liquid[interval,1],
     col=colors[3],axes=F,ylab="",xlab="",ylim=axisl)
points(interval,liquid[interval,2],col=colors[4])
axis(side=4, at = pretty(range(liquid[interval,1])))


# 3. Out-of-sample sharpe ratios with transaction costs
backtest <- function(beta) {
  
  vt <- function(t,w,beta,B) {
    tmp <- w*(1+logReturns[t,])
    wplus <- w*tmp/(t(iota) %*% tmp)[1]
    x <- (t(w-wplus) %*% B %*% (w-wplus))[1]
    beta*x/2
  }
  
  realized <- rep(0,250)
  w <- 1/N*matrix(1,nrow=N)
  for (t in 250:capT) {
    realized[t-249] <- (t(w) %*% logReturns[t,])[1] - vt(t,w,beta,B)
    
    Sigma <- cov(logReturns[(t-249):t,])
    w <- weights(t,w,mu,Sigma,gamma,beta,B,iota,T)
  }
  mean(realized)/sd(realized)
}  

betas <- seq(0,100,by=1)
sharpeBeta <- rep(0,Nbetas<-length(betas))
for (i in 1:Nbetas) sharpeBeta[i] <- backtest(betas[i])
plot(betas,sharpeBeta,xlab='Beta',ylab='Sharpe ratio',col='forestgreen')

# 4. Fully fledged portfolio backtesting strategy
beta <- 50/10000

objFun <- function(w,t,mu,Sigma,beta,gamma) {
    tmp <- w*(1+logReturns[t,])
    wplus <- w*tmp/(t(iota) %*% tmp)[1]
    
    L1v <- beta*sum(abs(w-wplus))
    (t(w) %*% mu)[1] - L1v - gamma*(t(w) %*% Sigma %*% w)[1]/2
}

cons <- function(w) {
  sum(w)-1
}

L1vFUN <- function(w) {
  tmp <- w*(1+logReturns[t,])
  wplus <- w*tmp/(t(iota) %*% tmp)[1]
  beta*sum(abs(w-wplus))
}

# Function from exercises for Ledoit-Wolf shrinkage estimator
compute_ledoit_wolf <- function(x) {
  # Computes Ledoit-Wolf shrinkage covariance estimator
  # This function generates the Ledoit-Wolf covariance estimator  as proposed in Ledoit, Wolf 2004 (Honey, I shrunk the sample covariance matrix.)
  # X is a (t x n) matrix of returns
  t <- nrow(x)
  n <- ncol(x)
  x <- apply(x, 2, function(x) if (is.numeric(x)) # demean x
    x - mean(x) else x)
  sample <- (1/t) * (t(x) %*% x)
  var <- diag(sample)
  sqrtvar <- sqrt(var)
  rBar <- (sum(sum(sample/(sqrtvar %*% t(sqrtvar)))) - n)/(n * (n - 1))
  prior <- rBar * sqrtvar %*% t(sqrtvar)
  diag(prior) <- var
  y <- x^2
  phiMat <- t(y) %*% y/t - 2 * (t(x) %*% x) * sample/t + sample^2
  phi <- sum(phiMat)
  
  repmat = function(X, m, n) {
    X <- as.matrix(X)
    mx = dim(X)[1]
    nx = dim(X)[2]
    matrix(t(matrix(X, mx, nx * n)), mx * m, nx * n, byrow = T)
  }
  
  term1 <- (t(x^3) %*% x)/t
  help <- t(x) %*% x/t
  helpDiag <- diag(help)
  term2 <- repmat(helpDiag, 1, n) * sample
  term3 <- help * repmat(var, 1, n)
  term4 <- repmat(var, 1, n) * sample
  thetaMat <- term1 - term2 - term3 + term4
  diag(thetaMat) <- 0
  rho <- sum(diag(phiMat)) + rBar * sum(sum(((1/sqrtvar) %*% t(sqrtvar)) * thetaMat))
  
  gamma <- sum(diag(t(sample - prior) %*% (sample - prior)))
  kappa <- (phi - rho)/gamma
  shrinkage <- max(0, min(1, kappa/t))
  if (is.nan(shrinkage)) shrinkage <- 1
  sigma <- shrinkage * prior + (1 - shrinkage) * sample
  return(sigma)
}

w_eq46 <- function(w,t) {
  tmp <- w*(1+logReturns[t,])
  w*tmp/(t(iota) %*% tmp)[1]
}

# Constraints for QP-problem
A <- cbind(1, diag(N))
b0 <- matrix(c(1, rep(0, N)),nrow=ncol(A))

# Initialize placeholders and naive portfolio weights
naive<-NS<-HV<-rep(0,250)
naiveEq46<-NSeq46<-HVeq46<-rep(0,249)
wNaive <- wNS <- wHV <- 1/N*matrix(1,nrow=N)
for (t in 250:capT) {
  
  HV[t-249] <- (t(wHV) %*% logReturns[t,])[1] - L1vFUN(wHV)
  NS[t-249] <- (t(wNS) %*% logReturns[t,])[1] - L1vFUN(wNS)
  naive[t-249] <- (t(wNaive) %*% logReturns[t,])[1] - L1vFUN(wNaive)
  
  tmp1 <- w_eq46(wHV,t)
  tmp2 <- w_eq46(wNS,t)
  tmp3 <- w_eq46(wNaive,t)
  
  # Covariance estimation
  Sigma <- compute_ledoit_wolf(logReturns[(t-249):t,])
  
  # Non-linear minimization (note minus in front of objFun) with old weights as initial values
  sol <- constrOptim.nl(wHV,fn=function(w) -objFun(w,t=t,mu=mu,Sigma=Sigma,beta=beta,gamma=gamma),
                      heq=cons,control.outer = list(trace=F))
  wHV <- sol$par
  
  # Minimizes -mu' %*% w + gamma/2 * w' %*% Sigma %*% w under constraint A' %*% w >= b0
  # Note that the first constraint is set as an equality and the rest as inequalities (meq = 1)
  sol <- solve.QP(Dmat=gamma*Sigma,dvec=mu,Amat=A, bvec=b0, meq=1)
  wNS <- sol$solution
  
  # See definition in eq 46: The sum starts at t=2. Hence, at t=251 in our setting.
  if (t>250) {
    HVeq46[t-250] <- sum(abs(wHV - tmp1))
    NSeq46[t-250] <- sum(abs(wNS - tmp2))
    naiveEq46[t-250] <- sum(abs(wNaive - tmp3))
  }
}

results <- data.frame(SR=c(mean(HV)/sd(HV),
                           mean(NS)/sd(NS),
                           mean(naive)/sd(naive)),
                      TO=c(mean(HVeq46),
                           mean(NSeq46),
                           mean(naiveEq46)))
rownames(results) <- c("HautschVoigt","No short","Naive")

# Copy paste into Latex
round(results,4) %>% kable(align = 'c', format = 'latex') 

